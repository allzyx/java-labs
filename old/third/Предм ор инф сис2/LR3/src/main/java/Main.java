import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter size of stack\n");
        int count = scanner.nextInt();
        StackTask stack = new StackTask(count);

        for ( char element : stack.getMyStack()){
           char symbol = scanner.next().charAt(0);
            stack.push(symbol);
        }

        System.out.println(stack.pop());
        System.out.println(stack.pop());


        System.out.println("======");

        stack.push('!');
        System.out.println(stack.pop());
    }
}
