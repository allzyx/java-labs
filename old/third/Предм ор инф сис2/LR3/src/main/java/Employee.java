public class Employee {
    double totalPayment;
    String name;
    double salary;
    double holidaysCoeff = 0.8;
    double illnessCoeff = 0.8;
    boolean isIll;
    boolean isOnHolidays;
    int childrenCount;
    double ndfl;
    double ndflCoeff = 0.13;


    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public Employee(String name, double salary, boolean isIll, boolean isOnHolidays, int childrenCount) {
        this.name = name;
        this.salary = salary;
        this.isIll = isIll;
        this.isOnHolidays = isOnHolidays;
        this.childrenCount = childrenCount;
    }

    double salaryToGet(){
        salary = salary - ndfl;
        return salary;
    }

     void findSalary(){
        if(isIll) {
            salary *= illnessCoeff;
        }

        if(isOnHolidays){
            salary *= holidaysCoeff;
        }
    }

    public double findNDFL(){
        if(childrenCount > 0 && childrenCount < 3){
            ndfl = (salary - 1400 * childrenCount) * ndflCoeff;
        }
        else{
            if(childrenCount > 3){
                ndfl = (salary - 1400 * 2 - 3000 * (childrenCount - 2)) * ndflCoeff;
            }
            else{
                ndfl = salary * ndflCoeff;
            }
        }
    return ndfl;
    }

    public double payments(){
        double nsnpCoeff = 0.002;
        double fssCoeff = 0.029;
        double fomsCoeff = 0.051;
        double pfrCoeff = 0.22;

        totalPayment = salary * nsnpCoeff + salary * fomsCoeff + salary * fssCoeff + salary * pfrCoeff;
        return totalPayment;
    }
}
