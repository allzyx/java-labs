public class StackTask implements Interfaces{
    private char [] myStack;
    private int stackPointer = 1;
    private int stackPosition = 0;
    private char symbol;
    private int absoluteStackSize;


    StackTask(int stackSize){
        myStack = new char[stackSize];
        absoluteStackSize = stackSize;
    }

    public char[] getMyStack() {
        return myStack;
    }

    public void push (char c){
        if(stackPosition >= absoluteStackSize){
            stackPosition--;
            myStack[stackPosition-1] = c;
            stackPointer--;
        }
        else{
        myStack[stackPosition] = c;
        stackPosition++;
        }
    }

    public char pop(){
        symbol = myStack[myStack.length - stackPointer];
        myStack[myStack.length - stackPointer] = ' ';
        absoluteStackSize--;
        stackPointer++;
        return symbol;
    }
}
