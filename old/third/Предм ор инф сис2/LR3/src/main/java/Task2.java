import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите фразу: ");
        String box = input.nextLine();
        String s = box;

        if(s.length() > 0) {
            reverseString(s, s.length() - 1);
        }
    }

    public static void reverseString(String s, int index) {
        if(index == 0) {
            System.out.println(s.charAt(index));

            return;
        }

        System.out.print(s.charAt(index));

        reverseString(s, index - 1);
    }

}
