import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Task31 {
    public static void main(String... sss) {

        Scanner scanner = new Scanner(System.in);
        String number;
        List<Double> list = new ArrayList<>();

        do{
            System.out.println("Enter the next number");
            number = scanner.next();
            try{
            list.add(Double.parseDouble(number));}
            catch (Exception ex){}
        } while(!number.equals("stop"));



        double[] a = new double[100];
        int i=0;
        for(double c : list){
            a[i] = c;
            i++;
            //
        }
        sum(a);
    }

    static void sum(double... numbers){
        final double sum = Arrays.stream(numbers).sum();
        final String lineSeparator = System.lineSeparator();

        System.out.printf(" сумма - %s" ,sum, lineSeparator);
    }
}
