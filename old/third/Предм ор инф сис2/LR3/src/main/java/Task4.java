public class Task4 {
    public static void main(String[] args) {
        double totalPayment = 0;
        Employee employee1 = new Employee("Arina s payment",60000,false,false,0);
        employee1.findNDFL();
        employee1.findSalary();
        System.out.println("Nachislennaya zp = " + employee1.getSalary());

        System.out.println(employee1.getName() + " - "+ employee1.salaryToGet());
        System.out.println("NDFL = " + employee1.findNDFL());
        totalPayment += employee1.payments();

        Employee employee2 = new Employee("Liza s payment", 70000,false, false,0);
        employee2.findNDFL();
        employee2.findSalary();
        System.out.println("Nachislennaya zp = " + employee2.getSalary());

        System.out.println(employee2.getName() + " - " +  employee2.salaryToGet());
        System.out.println("NDFL = " + employee2.findNDFL());
        totalPayment = totalPayment + employee2.payments();

        Employee employee3 = new Employee("Angelina s payment", 60000, false,false,0);
        employee3.findNDFL();
        employee3.findSalary();
        System.out.println("Nachislennaya zp = " + employee3.getSalary());

        System.out.println(employee3.getName() +" - " + employee3.salaryToGet());
        System.out.println("NDFL = " + employee3.findNDFL());
        totalPayment = totalPayment + employee3.payments();

        System.out.println("Total payment = " + totalPayment);

    }
}
