public class Quad {
    private double R;

    public Quad(double R) {
        this.R = R;
        double c = 2*3.14*R;
        double l = 3.14 * R * R;
        System.out.println("Length of circle = "+ c);
        System.out.println("Square of circle = "+ l);

    }
}
