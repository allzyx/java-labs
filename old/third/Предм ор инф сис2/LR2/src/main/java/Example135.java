import java.util.Scanner;

class Simple84a {
     private double width;
     private double height;


     Simple84a() {
         width = height = 0;

     }

     Simple84a(double width, double height) {
         this.width = width;
         this.height = height;
     }

     String detectStyle(double width, double height, double oneMoreSide){
         double hipotenus =  width * width + height * height;
         if(width == oneMoreSide){
             if(hipotenus == oneMoreSide){
                 return "Равнобедренный, прямоугольный треугольник";
             }
             else{
             if(height*height + width/2*width/2 == oneMoreSide){
                 return "Равносторонний треугольник";
             }
             else{
                 return "Равнобедренный треугольник";}
             }
         }
         else{
             if(hipotenus == oneMoreSide){
                 return "Прямоугольный треугольник";
             }
             else{return "Разносторонний треугольник";}
     }}

     Simple84a(double x) {
         width = height = x;
     }

     double getWidth() {
         return width;
     }

     void setWidth(double width) {
         this.width = width;
     }

     double getHeight() {
         return height;
     }

     void setHeight(double height) {
         this.height = height;
     }

     void showD() {
         System.out.println("Ширина и высота - " + width + " и " + height);
     }
 }

 class Simple841a extends Simple84a {
     private String style;

     Simple841a() {
         super();
         style = "null";
     }

     Simple841a(String s, double width, double height) {
         super(width, height);
         style = s;
     }

     Simple841a(double x) {
         super(x);
         style = "равнобедренный";
     }

     double area() {
         return getWidth() * getHeight() / 2;
     }

     void showStyle() {
         System.out.println("Треугольник - " + style);
     }
 }

 class Simple extends Simple841a {
     private String color;

     Simple(String c, String s, double w, double h) {
         super(s, w, h);
         color = c;
     }

     String getColor() {
         return color;
     }

     void showColor() {
         System.out.println("Цвет треугольника - " + color);
     }
 }

 class Simple842a{
     public static void main(String[] args){
         Simple t1 = new Simple("Красный","равнобедренный",2,2);
         Simple t2 = new Simple("Синий","прямоугольный", 8, 12);

         System.out.println("Данные о треугольнике 1:");
         t1.showStyle();
         t1.showD();
         t1.showColor();
         System.out.println("Площадь равна: " + t1.area());

         System.out.println("\nДанные о треугольнике 2:");
         t2.showStyle();
         t2.showD();
         t2.showColor();
         System.out.println("Площадь равна:" + t2.area());

         Simple84a t3 = new Simple84a(8, 12);
         System.out.println("Хотите ли вы определить тип треугольника?");
         Scanner scanner = new Scanner(System.in);
         String response = scanner.nextLine();
         String yes = "Yes";

         if(response.equals(yes)){
             t3.showD();
             System.out.println("Style: " + t3.detectStyle(t3.getWidth(), t3.getHeight(),8));
         }
     }


}
