class Example1 {
    double height;
    double width;

    void shD(){
        System.out.println("Ширина и высота:" + width + " и " + height);
    }
}

class Simple2 extends Example1{
    String sty;

    double ar(){
        return width * height/2;
    }

    void showS(){
        System.out.println("Треугольник - " + sty);
    }
}

class Simple2r {

    public  static void main(String [] args){
        Simple2 t1 = new Simple2();
        Simple2 t2 = new Simple2();

        t1.width = 4.0;
        t1.height = 4.0;
        t1.sty = "равнобедренный";

        t2.width = 8.0;
        t2.height = 12.0;
        t2.sty = "прямоугольный";

        System.out.println("Данные о треугольнике 1: ");
        t1.showS();
        t1.shD();
        System.out.println("Площадь равна: " + t1.ar());

        System.out.println("Данные о треугольнике 2: ");
        t2.showS();
        t2.shD();
        System.out.println("Площадь равна: " + t2.ar());
    }
}

