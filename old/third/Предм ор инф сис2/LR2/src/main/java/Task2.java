import java.util.Scanner;

public class Task2 {
    public static int factorial(int x) {
        if (x < 0) throw new IllegalArgumentException("x must be >=0");
        int fact = 1;
        for (int i = 2; i <= x; i++) {
            fact *= i;
        }
        return fact;
    }

    public static long bigFactorial(long x){
        if(x < 0) throw  new IllegalArgumentException("x must be >= 0");

        if(x<=1) return 1;
        else return x * bigFactorial(x-1);
    }


    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the number to calculate factorial");
            int i = scanner.nextInt();
            Task2 task2 = new Task2();
            System.out.println("Factorial of " + i + " = " + task2.factorial(i));

            System.out.println("Enter the number to calculate factorial");
            long j = scanner.nextLong();
            Task2 task21 = new Task2();
            System.out.println("Factorial of " + j + " = " + task21.bigFactorial(j));
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}