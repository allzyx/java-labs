 class Simple83 {

     private double width;
     private double height;


     Simple83() {
         width = height = 0;

     }

     Simple83(double width, double height) {
         this.width = width;
         this.height = height;
     }

     Simple83(double x) {
         width = height = x;
     }

     double getWidth() {
         return width;
     }

     void setWidth(double width) {
         this.width = width;
     }

     double getHeight() {
         return height;
     }

     void setHeight(double height) {
         this.height = height;
     }

     void showD() {
         System.out.println("Ширина и высота - " + width + " и " + height);
     }
 }


     class Simple831 extends Simple83{
        private String sty;

        Simple831(){
            super();
            sty = "null";
        }

        Simple831(String s, double width, double height){
            super(width, height);
            sty = s;
        }

        Simple831(double x){
            super(x);
            sty = "равнобедренный";
        }
    }

    class Simple81 {
            private double width;
            private double height;
            String sty;

        public double getWidth() {
            return width;
        }

        public void setWidth(double width) {
            this.width = width;
        }

        public double getHeight() {
            return height;
        }

        public void setHeight(double height) {
            this.height = height;
        }

        void showD() {
            System.out.println("Ширина и высота - " + width + " и " + height);
        }

        double ar(){
            return getWidth() * getHeight()/2;
        }

        void showSty(){
            System.out.println("Треугольник - " + sty);
        }
    }

    class Simple811 extends Simple81 {
        private String sty;

        Simple811(String s, double width, double height) {
            setWidth(width);
            setHeight(height);
            sty = s;
        }

        double ar() {
            return getWidth() * getHeight() / 2;
        }

        void showSty() {
            System.out.println("Треугольник - " + sty);
        }
    }

         class Simple812{
                public static void main(String[] args){
                    Simple811 t1 = new Simple811("равнобедренный",4,4);
                    Simple811 t2 = new Simple811("прямоугольный", 8, 12);

                    System.out.println("Данные о треугольнике 1:");
                    t1.showSty();
                    t1.showD();
                    System.out.println("Площадь равна: " + t1.ar());

                    System.out.println("\nДанные о треугольнике 2:");
                    t2.showSty();
                    t2.showD();
                    System.out.println("Площадь равна:" + t2.ar());
                }
        }




