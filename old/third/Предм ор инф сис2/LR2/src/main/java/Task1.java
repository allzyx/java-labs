public class Task1 extends Triangle{
    double fourCornerSquare;
    double fourCornerPerimetr;
    private double a4, d1, d2, gradus;

    public Task1(double a1, double a2, double a3, double a4) {
        super(a1, a2, a3);
        this.a4 = a4;
        double perimetr = a1 + a2 + a3 + a4;
        double square = a1*a2;
        System.out.println("Perimetr = " + perimetr);
        System.out.println("Ploshad = " + square);
    }

    double findfourCornerPloshad(double d1, double d2, double gradus){
        fourCornerSquare = (d1*d2*Math.sin(gradus)/2);
        return fourCornerSquare;
    }
}


