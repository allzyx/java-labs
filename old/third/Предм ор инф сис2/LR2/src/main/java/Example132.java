 class Example132 {
     private double width;
     private double height;

     void setWidth(double width) {
         this.width = width;
     }

     void setHeight(double height) {
         this.height = height;
     }

     double getWidth() {
         return width;
     }

     double getHeight() {
         return height;
     }

     void showD(){
         System.out.println("Ширина и высота - " + width + " и " + height);
     }

 }

        class Examplee1 extends Example132{
            String sty;
            double ar(){
                return getWidth() * getHeight()/2;
            }

            void showSty(){
                System.out.println("Треугольник - " + sty);
            }
        }

        class Example2r{
        public static void main(String [] args){
            Examplee1 simple1 = new Examplee1();
            Examplee1 simple2 = new Examplee1();

            simple1.setHeight(4);
            simple1.setWidth(4);
            simple1.sty = "равнобедренный";

            simple2.setWidth(8);
            simple2.setHeight(12);
            simple2.sty = "прямоугольный";

            System.out.println("Данные о треугольнике 1:");
            simple1.showSty();
            simple1.showD();
            System.out.println("Площадь равна: " + simple1.ar());

            System.out.println("\nДанные о треугольнике 2:");
            simple2.showSty();
            simple2.showD();
            System.out.println("Площадь равна:" + simple2.ar());

        }
        }


