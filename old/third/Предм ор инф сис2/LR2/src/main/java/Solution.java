class Solution {
    public static String whoLikesIt(String[] names) {
        String message = "";
        if (names.length == 0) {
            message = "no one likes this";
        }

       int i = names.length;
        if(names.length >= 4){
            message = names[0] + " and" +  Integer.toString(names.length - 1)
                    + "others like this";
        }

        if(names.length == 1){
            message = names[0] + " likes this";
        }

        if(names.length == 2){
            message = names[0] + " and " + names[1] + " like this";
        }

        if(names.length == 3){
            message = names[0] + ", " + names[1] + " and "
                    + names[2] + "like this";
        }
        //Do your magic here
return message;
    }
}