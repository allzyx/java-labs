import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first number");
        try {
            int n = scanner.nextInt();
            int i;
            int nextNum;
            System.out.println(n);
            for (i = 0; i < 20; i++) {
                nextNum = n + (n + 1) + (n + 2);
                System.out.println(nextNum);
                n++;
            }
            System.out.println("===============\nEnter count of number you want to found");
            int count = scanner.nextInt();
            System.out.println("Enter the first number");
            n = scanner.nextInt();
            try {
                for (i = 0; i < count; i++) {
                    if (i > count || i < 0) throw new IllegalArgumentException("value is out of range");
                    else {
                        nextNum = n + (n + 1) + (n + 2);
                        System.out.println(nextNum);
                        n++;
                    }
                }
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
