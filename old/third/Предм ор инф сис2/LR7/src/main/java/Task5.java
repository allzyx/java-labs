public class Task5 {
    public static void main(String[] args) {
        try{
            int i;
        for(i = 1; i < 16; i++){
            if(i < 0 || i > 15){throw new IllegalArgumentException("i must be in range [1;15]");}
            else{
            System.out.println(i);}
        }}
        catch(IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }

        System.out.println("========================");
        try {
            for (int i = 15; i >= 1; i = i - 2) {
                if(i < 0 || i > 15){throw new IllegalArgumentException("i must be in range [1;15]");}
                else{
                System.out.println(i);}
            }
        }
        catch(IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
    }
}
