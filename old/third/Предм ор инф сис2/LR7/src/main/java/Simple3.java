public class Simple3 {
    int i, j;
    Simple3(int a, int b){
        i = a;
        j = b;
    }

    void show(){
        System.out.println("i and j: " + i + " " + j);
    }

    final void meth() {
        System.out.println("This method is final");
    }
}



class Simple4 extends Simple3{
    int k;
    Simple4(int a, int b, int c){
        super(a,b);
        k = c;
    }

    //This method can t be overridden because of his definition as final
   /* final void meth(){
        System.out.println("This method is final");
    }*/

    void show(String message){
        System.out.println(message + k);
    }
}



