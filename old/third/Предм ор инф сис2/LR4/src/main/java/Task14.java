import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task14 {
    public static void main(String [] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your message");
        String message = scanner.nextLine();
        System.out.println("Choose the style, where:" +
                "\n1 - по левому краю" +
                "\n2 - по правому краю" +
                "\n3 - по центру" +
                "\n4 - по ширине");
        int number = scanner.nextInt();
        String regex1 = "([\\w]+)[\\s]?[\\S]";
        String regex2 = "[\\s]{10}([\\w]+)?[\\S]";
        if (number == 1) {
            Pattern pattern = Pattern.compile(regex1);
            Matcher matcher = pattern.matcher(message);
            String finalString = "";

            while (matcher.find()) {
                finalString = finalString + message.substring(matcher.start(), matcher.end());
            }
            System.out.println(finalString);
        }

        if (number == 2) {
            System.out.printf("%150s",message);
        }

        if(number == 3 ){
            System.out.printf("%75s", message);
        }

        if (number == 4 ) {
            String[] wordArray = message.split(" ");
            String newString = "";
            for (String word : wordArray) {
                newString = newString + word + "                       ";
            }
            System.out.println(newString);
        }


    }
}
