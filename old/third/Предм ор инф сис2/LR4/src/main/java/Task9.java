import java.util.Scanner;

public class Task9 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the date in format of dd.MM.YYYY");
        String date = scanner.nextLine();
        String[] stringArray = date.split("\\.");
        System.out.println(stringArray[0] + " - day");
        System.out.println(stringArray[1] + " - month");
        System.out.println(stringArray[2] + " - years");
    }
}
