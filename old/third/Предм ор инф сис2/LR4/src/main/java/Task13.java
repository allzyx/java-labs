import java.util.HashMap;
import java.util.Scanner;

public class Task13 {
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter string you want to change");
        String message = scanner.nextLine();

        HashMap<String, String> letters = new HashMap<>();
        letters.put("А", "Гласная");
        letters.put("Б", "Согласная");
        letters.put("В", "Согласная");
        letters.put("Г", "Согласная");
        letters.put("Д", "Согласная");
        letters.put("Е", "Гласная");
        letters.put("Ё", "Гласная");
        letters.put("Ж", "Согласная");
        letters.put("З", "Согласная");
        letters.put("И", "Гласная");
        letters.put("Й", "Гласная");
        letters.put("К", "Согласная");
        letters.put("Л", "Согласная");
        letters.put("М", "Согласная");
        letters.put("Н", "Согласная");
        letters.put("О", "Гласная");
        letters.put("П", "Согласная");
        letters.put("Р", "Согласная");
        letters.put("С", "Согласная");
        letters.put("Т", "Согласная");
        letters.put("У", "Гласная");
        letters.put("Ф", "Согласная");
        letters.put("Х", "Согласная");
        letters.put("Ц", "Согласная");
        letters.put("Ч", "Согласная");
        letters.put("Ш", "Согласная");
        letters.put("Щ", "Согласная");
        letters.put("Ъ", "Согласная");
        letters.put("Ы", "Гласная");
        letters.put("Ъ", "Согласная");
        letters.put("Э", "Гласная");
        letters.put("Ю", "Гласная");
        letters.put("Я", "Гласная");
        letters.put("а", "Гласная");
        letters.put("б", "Согласная");
        letters.put("в", "Согласная");
        letters.put("г", "Согласная");
        letters.put("д", "Согласная");
        letters.put("е", "Гласная");
        letters.put("ё", "Гласная");
        letters.put("ж", "Согласная");
        letters.put("з", "Согласная");
        letters.put("и", "Гласная");
        letters.put("й", "Гласная");
        letters.put("к", "Согласная");
        letters.put("л", "Согласная");
        letters.put("м", "Согласная");
        letters.put("н", "Согласная");
        letters.put("о", "Гласная");
        letters.put("п", "Согласная");
        letters.put("р", "Согласная");
        letters.put("с", "Согласная");
        letters.put("т", "Согласная");
        letters.put("у", "Гласная");
        letters.put("ф", "Согласная");
        letters.put("х", "Согласная");
        letters.put("ц", "Согласная");
        letters.put("ч", "Согласная");
        letters.put("ш", "Согласная");
        letters.put("щ", "Согласная");
        letters.put("ъ", "Согласная");
        letters.put("ы", "Гласная");
        letters.put("ъ", "Согласная");
        letters.put("э", "Гласная");
        letters.put("ю", "Гласная");
        letters.put("я", "Гласная");

        int countOfGlasnyh = 0;
        int countOfSoglasnyh= 0;

        char[] symbolArray = message.toCharArray();

        for(char symbol : symbolArray){
            if (letters.containsKey(Character.toString(symbol))){
            String result = letters.get(Character.toString(symbol));
            if(result.equals("Гласная")){
                countOfGlasnyh++;
            }
            else{
                countOfSoglasnyh++;
            }
        }}
        System.out.println(message.length());
        System.out.println("Кол во соглассных в строке = " + countOfSoglasnyh);
        System.out.println("Кол во глассных в строке = " + countOfGlasnyh);
    }
}
