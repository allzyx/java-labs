import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringType {
    public static void main(String[] args) {
        //TODO: Task 1
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the string with whitespaces before first and after the last word");
        String message = scanner.nextLine();

        String changedMessage = message.strip();
        System.out.println(changedMessage);

        //TODO: Task 2

        System.out.println("Enter count of symbols in string");
        int i = scanner.nextInt();
        String regex1 = "([\\w]+)[\\s]?[\\S]";
        Pattern pattern = Pattern.compile(regex1);
        Matcher matcher = pattern.matcher(message);
        String finalString = "";

        while (matcher.find()) {
            finalString = finalString + message.substring(matcher.start(), matcher.end());
        }

        if (finalString.length() < i) {
            String[] a = finalString.split("\\s");

            for (int j = 0; j < i - finalString.length() + 1; j++) {
                a[0] = a[0] + " ";
            }

            finalString = a[0] + a[1];
            for (int c = 2; c < a.length; c++) {
                finalString = finalString + " " + a[c];
            }
        }
        System.out.println(finalString);
        System.out.println(finalString.length());

    }
}
