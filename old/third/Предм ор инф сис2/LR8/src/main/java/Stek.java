public class Stek {
    int size;
    char [] symbols;
    int i = 0;
    int n;

    public Stek(int size){
        this.size = size;
        symbols = new char[size];
        n = size;
    }

    void push(char symbol){
        symbols[i] = symbol;
        i++;
    }

    char[] getAll(){
        return symbols;
    }

    char get(){
        char symbol = symbols[n - 1];
        n--;
        if(n < 0){
            symbols = new char[size];
            symbol = symbols[n-1];
        }
        return symbol;
        }
    }


