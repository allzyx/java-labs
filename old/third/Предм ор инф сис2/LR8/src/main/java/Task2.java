public class Task2 {
        int a; // действительная часть
        int b; // мнимая часть

        public Task2(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public Task2 add(Task2 num1, Task2 num2) {
            return new Task2(num1.a + num2.a, num1.b + num2.b);
        }

        public Task2 mul(Task2 num1, Task2 num2) {
            int a = num1.a, b = num1.b, c = num2.a, d = num2.b;
            return new Task2(a * c - b * d, b * c + a * d);
        }

    @Override
    public String toString() {
        return a + "+" + b + "i";
    }

        // tests
        public static void main(String[] args) {
            Task2 a = new Task2(2, 2);
            Task2 b = new Task2(1,1);
            System.out.println(a.add(a, b));
            System.out.println(b.mul(a, b));
        }

}
