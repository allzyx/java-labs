public class Ochered10 {
    public static void main(String[] args) {
        Ochered9 q1 = new Ochered9(10);
        char ch;
        int i;
        try{
            for(i = 0; i < 11; i++){
                if(i <= 11){
                System.out.println("Saving attempt : " + (char)('A' + i));
                System.out.println(" - OK");}
                else{
                    throw new Ochered7(11);
                }
            }
            System.out.println();
        }
        catch (Ochered7 ex){
            System.out.println(ex);
        }

        try{
            for(i = 0; i < 11; i++){
                System.out.println("Getting the next symbol: ");
                ch = q1.get();
                System.out.println(ch);
            }
        }
        catch (Ochered8 ex){
            System.out.println(ex);
        }
    }
}
