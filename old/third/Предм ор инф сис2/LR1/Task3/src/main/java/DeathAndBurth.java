
import java.util.HashMap;

public class DeathAndBurth {

    HashMap<String,Integer> findTopOfMax(HashMap<String,Integer> hashMap){
        HashMap<String,Integer>topOfTheMax = new HashMap<>();

        for(int i = 0; i < 3; i++){
        String maxKey = hashMap.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue()
                    ? 1 : -1).get().getKey();
            int maxValue = hashMap.get(maxKey);
            topOfTheMax.put(maxKey,maxValue);
            hashMap.remove(maxKey);}
        return topOfTheMax;
}

    HashMap<String,Integer> findTopOfMin(HashMap<String,Integer> hashMap){
        HashMap<String,Integer>topOfTheMin = new HashMap<>();

        for(int i = 0; i < 3; i++){
            String minKey = hashMap.entrySet().stream().min((entry1, entry2) -> entry1.getValue() < entry2.getValue()
                    ? -1 : 1).get().getKey();
            int minValue = hashMap.get(minKey);
            topOfTheMin.put(minKey,minValue);
            hashMap.remove(minKey);}
        return topOfTheMin;
    }

}
