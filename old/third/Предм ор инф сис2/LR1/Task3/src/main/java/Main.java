import java.util.*;

public class Main {
    public static void main(String[] args) {

        int year = 2000;
        int burths[] = {1266800, 1311604, 1396967, 1477301, 1502477, 1457376, 1479637, 1610122, 1713947,
                1761687, 1788948, 1796629, 1902084, 1895822, 1942683, 1940579,
                1888729, 1690307, 1604344, 1481074};
        int deaths[] = {2225332, 2254856, 2332272, 2365826, 2295402, 2303935, 2166703, 2080445,
                2075954, 2010543, 2028516, 1925720, 1906335, 1871809,
                1912347, 1908541, 1891015, 1826125, 1828910, 17983007};

        HashMap <String, Integer> deathList = new HashMap<>();

        for (int death : deaths){
            String i = Integer.toString(year);
            deathList.put(i, death);
            year++;
        }

        year = 2000;
        HashMap<String, Integer> burthList = new HashMap<>();
        for (int burth : burths){
            String i = Integer.toString(year);
            burthList.put(i, burth);
            year++;
        }

        System.out.println("list of deaths" + deathList);
        System.out.println("list of burths" + burthList);

        HashMap<String,Integer> topOfTheBurths = new HashMap<>();
        HashMap<String,Integer> topOfTheDeaths = new HashMap<>();
        HashMap<String,Integer> theLowestBurths = new HashMap<>();
        HashMap<String,Integer> theLowestDeaths = new HashMap<>();




        DeathAndBurth deathAndBurth = new DeathAndBurth();
        topOfTheBurths = deathAndBurth.findTopOfMax(burthList);
        topOfTheDeaths = deathAndBurth.findTopOfMax(deathList);
        theLowestBurths = deathAndBurth.findTopOfMin(burthList);
        theLowestDeaths = deathAndBurth.findTopOfMin(deathList);


        System.out.println("3 года с самой высокой рождаемостью:\t" + topOfTheBurths);
        System.out.println("3 года с самой высокой смертностью:\t" + topOfTheDeaths);
        System.out.println("3 года с самой низкой рождаемостью:\t" + theLowestBurths);
        System.out.println("3 года с самой низкой смертностью:\t" + theLowestDeaths);

    }
}
