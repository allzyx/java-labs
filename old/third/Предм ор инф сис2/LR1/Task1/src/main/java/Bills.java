public class Bills {
    static double NDFL;
    static double propertyTax;
    static double income;
    static double cadastralValue;

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getCadastralValue() {
        return cadastralValue;
    }

    public void setCadastralValue(double cadastralValue) {
        this.cadastralValue = cadastralValue;
    }

    public double findNDFL (double income){
        NDFL = this.income * 0.13;
        return NDFL;
    }


    public double findPropertyTax (double cadastralValue){
        propertyTax = this.cadastralValue * 0.01;
        return propertyTax;
    }
}
