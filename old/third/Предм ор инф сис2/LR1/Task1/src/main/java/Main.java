import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Bills bills = new Bills();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите кадастровую стоимость имущества");
        bills.setCadastralValue(scanner.nextDouble());
        System.out.println("Введите доход физического лица");
        bills.setIncome(scanner.nextDouble());

        System.out.println("НДФЛ для физического лица с доходом = " + bills.getIncome() + " составил " +
                bills.findNDFL(bills.getIncome()) + " руб.");

        System.out.println("Налог на имущество стоимость которого составляет " + bills.getCadastralValue()
                + " составил " + bills.findPropertyTax(bills.getCadastralValue()) + " руб.");

        Bills bills1 = new Bills();
        System.out.println("Введите доход сотрудника предприятия ");
        bills1.setIncome(scanner.nextDouble());
        System.out.println("НДФЛ для работника предприятия с доходом = " + bills1.getIncome()
                + " составил " + bills1.findNDFL(bills1.getIncome()) + " руб.");
    }
}
