public class Example5 implements Ochered1{
    private char q[];
    private int putlock, getlock;

    public Example5(int size) {
        q = new char[size+1];
        putlock = getlock = 0;
    }

    public void put(char ch){
        if(putlock == q.length-1) {
            putlock = 0;
        }
        putlock++;
        q[putlock]=ch;
    }
    public char get() {
        if (getlock == putlock){
            System.out.println(" - Очередь пуста");
            return (char) 0;
        }
        getlock++;
        if (getlock == putlock) {
            getlock = 0;
            return q[putlock];
        }
        return q[getlock];
    }
    public void reset(){
        putlock = getlock = 0;
    }

    public char[] getQ() {
        return q;
    }
}