public class Task2 implements Ochered1{
    private static char q[];
    private int putlock, getlock;

    public Task2(int size) {
        q = new char[size+1];
        putlock = getlock = 0;
    }
    public void put(char ch) {
        if (putlock == q.length - 1){
            char t[] = new char[q.length*2];
            for (int i = 0; i < q.length; i++) {
                t[i] = q[i];
            }
            q = t;
        }
        putlock++;
        q[putlock] = ch;
    }
    public char get() {
        if (getlock == putlock){
            System.out.println(" - Очередь пуста");
            return (char) 0;
        }
        getlock++;
        if (getlock == putlock) {
            getlock = 0;
            return q[putlock];
        }
        return q[getlock];
    }
    public void reset(){
        putlock = getlock = 0;
    }

    static void copy(char[] ch){
        int i = q.length;
        char [] q1 = new char[q.length];
        int number = 0;
        for(char c : q){
            q1[number] = c;
            number++;
        }

        number = 0;
        q = new char[i + ch.length];
        for(char c : q1){
            q[number] = c;
            number++;
        }
        
        for(char c : ch){
            q[number] = c;
            number++;
        }

    }

    public char[] getQ() {
        return q;
    }
}
