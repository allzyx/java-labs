public class Example41 implements Ochered1{
    private char q[];
    private int putlock, getlock;

    public Example41(int size) {
        q = new char[size+1];
        putlock = getlock = 0;
    }
    public void put(char ch){
        if (putlock == q.length - 1){
            char t[] = new char[q.length*2];
            for (int i = 0; i < q.length; i++) {
                t[i] = q[i];
            }
            q = t;
        }
        putlock++;
        q[putlock] = ch;
    }
    public char get() {
        if (getlock == putlock) {
            System.out.println("- Очередь пуста");
            return (char)0;
        }
        getlock++;
        return q[getlock];
    }
    public void reset(){
        putlock = getlock = 0;
    }

}