import java.util.Scanner;

public class Task12 {
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter string you want to change");
        String messageBefore = scanner.nextLine();
        String messageAfter = messageBefore.trim().replaceAll(" +", " ");

        System.out.println(messageAfter);
    }
}
