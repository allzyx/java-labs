public class Task3 {
    public static String center(String text, int len){
        String out = String.format("%"+len+"s%s%"+len+"s", "",text,"");
        float mid = (out.length()/2);
        float start = mid - (len/2);
        float end = start + len;
        return out.substring((int)start, (int)end);
    }

    public static void main(String[] args) throws Exception{

        String s = "Some info";
        for (int i = 1; i < 200;i++){
            for (int j = 1; j < s.length();j++){

                System.out.println(center(s.substring(0, j),i));
            }
        }
    }
}
