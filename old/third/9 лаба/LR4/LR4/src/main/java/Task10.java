import java.util.Scanner;

public class Task10 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        String message = scanner.nextLine();
        System.out.println("Count of symbols in this string is " + message.length());

        String [] wordArray = message.split(" ");
        System.out.println("There are  " + wordArray.length + " words in this string");

        int max = wordArray[0].length();
        String maxWord = wordArray[0];
        int min = wordArray[0].length();
        String minWord = wordArray[0];

        for(String word : wordArray){
            if(word.length() > max){
                max = word.length();
                maxWord = word;
            }
            if(word.length() < min){
                min = word.length();
                minWord = word;
            }
        }

        System.out.println("The biggest word is - " + maxWord + ", his length = " + max);
        System.out.println("The most small word is - " + minWord + ", his length = " + min);

    }
}
