public class Task6 {
    public static void main(String[] args){
        String message = "Some info";
        char[] charArray = message.toCharArray();
        String codedMessage = "";

        for(char symbol : charArray){
            int unicodeNumber = symbol;
            int newUnicodeNumber = unicodeNumber + 1;
            char codedSymbol = (char)(newUnicodeNumber);
            //System.out.println(unicodeNumber);
            codedMessage = codedMessage + codedSymbol;
        }
        System.out.println(codedMessage);

        char[] charArray2 = codedMessage.toCharArray();
        String uncodedMessage = "";
        for(char symbol : charArray2){
            int unicodeNumber = symbol;
            int newUnicodeNumber = unicodeNumber - 1;
            char uncodedSymbol = (char)(newUnicodeNumber);
            uncodedMessage = uncodedMessage + uncodedSymbol;
        }
        System.out.println(uncodedMessage);
    }
}
