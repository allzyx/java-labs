import java.util.Scanner;

class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите два числа для расчета факториала: ");
        int x = scanner.nextInt();
        long y = scanner.nextLong();

        if (x < 0) {
            System.out.println("x должен быть >=0");
        } else {
            System.out.println("Факториал числа " + x + " = " + factorial(x));
        }
        if (y < 0) {
            System.out.println("y должен быть >=0");
        } else {
            System.out.println("Факториал числа " + y + " = " + factorial(y));
        }
        scanner.close();
    }

    public static int factorial(int x) {
        try {
            if (x < 0) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            return -1;
        }
        int fact = 1;
        for (int i = 2; i <= x; i++)
            fact *= i;
        return fact;
    }

    public static long factorial(long y) {
        try {
            if (y < 0) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            return -1;
        }
        if (y <= 1)
            return 1;
        else
            return y * factorial(y - 1);
    }
}