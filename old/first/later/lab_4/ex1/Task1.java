package lab_4.ex1;

import java.io.IOException;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose genre:\n1. Horror\n2. Comedy\n3. 3D-films");
        System.out.println("4. Multiplication\n5. Adventure\n");
        int genre = scanner.nextInt();

        double discountKids = 1;
        double discountTeens = 1;
        double discountYoung = 1;
        double discountOld = 1;

        switch (genre) {
            case 1:
                discountKids = 0;
                discountTeens = 0.3;
                discountYoung = 0.5;
                discountOld = 0.7;
                break;
            case 2:
                discountKids = 0;
                discountTeens = 0.5;
                discountYoung = 0.7;
                discountOld = 0.8;
                break;
            case 4:
                discountKids = 0;
                discountTeens = 0.3;
                discountYoung = 0.7;
                discountOld = 0.7;
                break;
            case 5:
                discountKids = 0;
                discountTeens = 0.3;
                discountYoung = 0.5;
                discountOld = 0.7;
                break;
            default:
                System.out.println("Illegal input!");
                scanner.close();
                return;
        }

        double priceKids = 0;
        double priceTeens = 0;
        double priceYoung = 0;
        double priceMiddle = 0;
        double priceOld = 0;
        double priceTotal = 0;

        int countKids = 0;
        int countTeens = 0;
        int countYoung = 0;
        int countMiddle = 0;
        int countOld = 0;

        int age;
        char addVisitor = 'y';
        double ticketPrice = 300;
        double yourPrice;

        while (addVisitor == 'y') {
            System.out.print("\nEnter your age: ");
            age = scanner.nextInt();

            if ((genre == 1 && age < 16) || (genre == 2 && age < 12)) {
                System.out.println("\nYou can't go on this film!");
                System.out.println("You want to add another visitor? y/n ");
                addVisitor = (char) System.in.read();
                continue;
            }
            else if (age < 7) {
                yourPrice = ticketPrice * discountKids;
                priceKids += yourPrice;
                priceTotal += yourPrice;
                countKids++;
            }
            else if (age < 18) {
                yourPrice = ticketPrice * discountTeens;
                priceTeens += yourPrice;
                priceTotal += yourPrice;
                countTeens++;
            }
            else if (age < 24) {
                yourPrice = ticketPrice * discountYoung;
                priceYoung += yourPrice;
                priceTotal += yourPrice;
                countYoung++;
            }
            else if (age < 65) {
                yourPrice = ticketPrice;
                priceMiddle += yourPrice;
                priceTotal += yourPrice;
                countMiddle++;
            }
            else if (age >= 65) {
                yourPrice = ticketPrice * discountOld;
                priceOld += yourPrice;
                priceTotal += yourPrice;
                countOld++;
            }


            System.out.println("You want to add another visitor? y/n ");
            addVisitor = (char) System.in.read();
        }
        System.out.println();
        System.out.println("Kids   [0; 7):   " + countKids + "\tPrice for kids: " + priceKids);
        System.out.println("Teens  [7; 18):  " + countTeens + "\tPrice for teens: " + priceTeens);
        System.out.println("Young  [18; 24): " + countYoung + "\tPrice for young: " + priceYoung);
        System.out.println("Middle [24; 65): " + countMiddle + "\tPrice for middle: " + priceMiddle);
        System.out.println("Old    [65+]:    " + countOld + "\tPrice for old: " + priceOld);
        System.out.println("Price total:     " + priceTotal);
        System.out.println();
        scanner.close();
    }
}
