package lab_4.ex2;

import java.util.Scanner;

public class WeightOnPlanetsMethods {
    public int getWeight() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your weight: ");
        int weight = scanner.nextInt();
        System.out.println();
        scanner.close();
        return weight;
    }

    public int getPlanet() {
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("1. Mercury\t2. Venus\t3. Moon");
        System.out.println("4. Mars\t\t5. Jupiter\t6. Saturn");
        System.out.println("7. Uran\t\t8. Neptune\t9. Pluto");
        System.out.print("Choose planet: ");
        int planet = scanner1.nextInt();
        System.out.println();
        scanner1.close();
        return planet;
    }

    public int calculateWeight(int weight, int planet) {
                switch (planet) {
            case 1:
                weight *= 0.378;
                break;
            case 2:
                weight *= 0.907;
                break;
            case 3:
                weight *= 0.166;
                break;
            case 4:
                weight *= 0.377;
                break;
            case 5:
                weight *= 2.364;
                break;
            case 6:
                weight *= 0.916;
                break;
            case 7:
                weight *= 0.889;
                break;
            case 8:
                weight *= 1.125;
                break;
            case 9:
                weight *= 0.067;
                break;
            default:
                System.out.println("Illegal input!");
        }
        return weight;
    }

    public void printWeight(int weight) {
        System.out.println("Your weight on this planet will be: " + weight);
    }
}
