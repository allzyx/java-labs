package lab_4.ex2;

public class Main {
    public static void main(String[] args) {
        WeightOnPlanetsMethods methods = new WeightOnPlanetsMethods();
        int weight = methods.getWeight();
        int planet = methods.getPlanet();
        int newWeight = methods.calculateWeight(weight, planet);
        methods.printWeight(newWeight);
    }
}
