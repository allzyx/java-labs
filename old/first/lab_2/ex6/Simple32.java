package lab_2.ex6;

public class Simple32 {
    public static void main(String[] args)
    throws java.io.IOException {
        int guess, answer = '6';
        System.out.println("Try to guess number from 1 to 10: ");
        guess = (int) System.in.read();
        if (guess == answer) System.out.println("You won!");
        else {
            System.out.println("I'm sorry, you are wrong!");
            if (guess < answer) System.out.println("Try bigger number");
            else System.out.println("Try smaller number");
        }
    }
}
