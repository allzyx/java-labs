package lab_2.ex1;

public class Simple15 {
    public static void main(String args[]) {
    int x;
    x = 10;
    int y = 20; // fix
    if (x==10) {
      //int y = 20;  // error
      System.out.println("x and y = " + x + " " + y);
      x = y*2;
    }
    y = 100;
    System.out.println("x = " + x);
  }
}
// variable scope error
// y declared inside if can't be changed outside if statement
