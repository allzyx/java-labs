package lab_2.ex3;

public class Simple23 {
    public static void main(String[] args) {
        long L;
        double D;
        D = 100123285.0;
        // L = D;
        L = (long)D;
        System.out.println("L and D: " + L + " " + D);
    }
}
// Long is is for integer numbers
// D is floating point number
// explicit type cast D to long to prevent error
