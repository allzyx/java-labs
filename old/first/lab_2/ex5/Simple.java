package lab_2.ex5;
public class Simple {
    public static void main(String[] args) {
        char ch1, ch2;
        ch1 = 'a';
        ch2 = 'b';
        // ch1 = ch1 + ch2  wrong line
        /*  we treat ch1 and ch2 as integers
            so we get result as integer as well and trying
            to assign result to variable with type char
            we have to explicitly recast result as char */
        ch1 = (char) (ch1 + ch2); // correct line
    }
}
