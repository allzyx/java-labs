package lab_2.ex7;

import java.io.IOException;
import java.util.Scanner;

public class OddOrEven {
    public static void main(String[] args)
    throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();
        scanner.close();
        System.out.println();

        if (number % 2 == 0) System.out.println(number + " is even");
        else System.out.println(number + " is odd");
    }
}
