package lab_3.ex4;
import java.util.Scanner;

public class Tickets {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your age: ");
        int age = scanner.nextInt();
        scanner.close();
        double discount = 1;
        double price = 300;

        if (age < 7) {
            discount = 0;
        }
        else if (age < 18) {
            discount = 0.3;
        }
        else if (age < 24) {
            discount = 0.5;
        }
        else if (age > 65) {
            discount = 0.7;
        }

        System.out.println("Price of a ticket: " + price);
        System.out.println("Price for you: " + price * discount);
    }
}
