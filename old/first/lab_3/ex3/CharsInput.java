package lab_3.ex3;
import java.io.IOException;

public class CharsInput {
    public static void main(String[] args) throws IOException {
        char input = '0';
        int spaceCount = 0;
        System.out.println("Enter a symbol, '.' to quit: ");
        while (input != '.') {
            input = (char) System.in.read();
            if (input == ' ') {
                spaceCount++;
            }
        }
        System.out.println("Space count = " + spaceCount);
    }
}
