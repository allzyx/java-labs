package lab_3.ex1;
import java.util.Scanner;

public class WeightOnPlanets {
   public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your weight: ");
        double weight = scanner.nextInt();
        System.out.println();

        System.out.println("1. Mercury\t2. Venus\t3. Moon");
        System.out.println("4. Mars\t\t5. Jupiter\t6. Saturn");
        System.out.println("7. Uran\t\t8. Neptune\t9. Pluto");
        System.out.print("Choose planet: ");
        int planet = scanner.nextInt();
        System.out.println();
        scanner.close();

        switch (planet) {
            case 1:
                weight *= 0.378;
                break;
            case 2:
                weight *= 0.907;
                break;
            case 3:
                weight *= 0.166;
                break;
            case 4:
                weight *= 0.377;
                break;
            case 5:
                weight *= 2.364;
                break;
            case 6:
                weight *= 0.916;
                break;
            case 7:
                weight *= 0.889;
                break;
            case 8:
                weight *= 1.125;
                break;
            case 9:
                weight *= 0.067;
                break;
            default:
                System.out.println("Illegal input!");
        }

        System.out.println("Your weight on this planet will be: " + weight);
    }
}
