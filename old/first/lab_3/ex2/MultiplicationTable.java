package lab_3.ex2;
import java.util.Scanner;

public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number from 1 to 9: ");
        int number = scanner.nextInt();
        System.out.println();

        switch (number) {
            case 1:
                for (int i = 1; i < 11; i++) {
                    System.out.println("1 * " + i + " = " + 1 * i);
                }
                break;
            case 2:
                for (int i = 1; i < 11; i++) {
                    System.out.println("2 * " + i + " = " + 2 * i);
                }
                break;
            case 3:
                for (int i = 1; i < 11; i++) {
                    System.out.println("3 * " + i + " = " + 3 * i);
                }
                break;
            case 4:
                for (int i = 1; i < 11; i++) {
                    System.out.println("4 * " + i + " = " + 4 * i);
                }
                break;
            case 5:
                for (int i = 1; i < 11; i++) {
                    System.out.println("5 * " + i + " = " + 5 * i);
                }
                break;
            case 6:
                for (int i = 1; i < 11; i++) {
                    System.out.println("6 * " + i + " = " + 6 * i);
                }
                break;
            case 7:
                for (int i = 1; i < 11; i++) {
                    System.out.println("7 * " + i + " = " + 7 * i);
                }
                break;
            case 8:
                for (int i = 1; i < 11; i++) {
                    System.out.println("8 * " + i + " = " + 8 * i);
                }
                break;
            case 9:
                for (int i = 1; i < 11; i++) {
                    System.out.println("9 * " + i + " = " + 9 * i);
                }
                break;
            default:
                System.out.println("Illegal input!");
        }

        scanner.close();
    }
}
