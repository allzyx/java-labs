enum Transport { CAR, TRUCK, AIRPLANE, TRAIN, BOAT, BUS }

public class Ex2 {
  public static void main(String args[]) {
    Transport tp;
    System.out.println("Константы Transport:");
    Transport allTrans[] = Transport.values();
    for (Transport t : allTrans)
      System.out.println(t);
    System.out.println();
    tp = Transport.valueOf("BOAT");
    System.out.println("tp содержит " + tp);
  }
}
