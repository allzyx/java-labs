enum Transport2 {
  CAR(65),
  TRUCK(55),
  AIRPLANE(600),
  TRAIN(70),
  BOAT(22),
  BUS(40);
  private int avspeed;

  Transport2(int s) { avspeed = s; }

  int getAvspeed() { return avspeed; }
}
