import java.util.Scanner;

public class Enum_1 {
  public static void main(String[] args) {

    Scanner scaner = new Scanner(System.in);
    Transport tp;
    int price = 0;
    System.out.println("Choose transport: ");
    System.out.println("CAR   TRUCK   AIRPLANE\nTRAIN   BOAT   BUS");
    tp = scaner.nextLine();
    System.out.println("You chose = " + tp);
    System.out.println();

    switch (tp) {
    case CAR:
      System.out.println("Car transfers people");
      price = 100;
      break;
    case TRUCK:
      System.out.println("Truck transfers goods. You can't travel this way.");
      System.out.println("Try looking into car and airplane.");
      return;
    case AIRPLANE:
      System.out.println("Airplane flies");
      price = 200;
      break;
    case TRAIN:
      System.out.println("Train drives on rails");
      price = 70;
      break;
    case BOAT:
      System.out.println("Boat transfers on water");
      price = 120;
      break;
    case BUS:
      System.out.println("Bus is like car but bigger");
      price = 120;
      break;
    default:
      System.out.println("Invalid transoprt");
      return;
    }
    int distance = 0;
    System.out.println("Enter distance: ");
    distance = scaner.nextInt();
    System.out.println("Price = " + price * distance);
  }
}
