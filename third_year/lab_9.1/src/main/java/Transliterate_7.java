import java.util.Scanner;

public class Transliteration {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Input string: ");
    String input = scanner.nextLine();
    String transliterated = transliterate(input);
    System.out.println("Transliterated: " + transliterated);
  }

  public static String transliterate(String input) {
    char[] cyrillicChars = {
        'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М',
        'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ',
        'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з',
        'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х',
        'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};

    String[] latinTranslit = {
        "A",  "B",  "V",  "G",  "D",   "E", "E", "Zh", "Z", "I",  "Y",
        "K",  "L",  "M",  "N",  "O",   "P", "R", "S",  "T", "U",  "F",
        "Kh", "Ts", "Ch", "Sh", "Sch", "",  "Y", "",   "E", "Yu", "Ya",
        "a",  "b",  "v",  "g",  "d",   "e", "e", "zh", "z", "i",  "y",
        "k",  "l",  "m",  "n",  "o",   "p", "r", "s",  "t", "u",  "f",
        "kh", "ts", "ch", "sh", "sch", "",  "y", "",   "e", "yu", "ya"};

    StringBuilder result = new StringBuilder();

    for (char c : input.toCharArray()) {
      int index = -1;
      for (int i = 0; i < cyrillicChars.length; i++) {
        if (c == cyrillicChars[i]) {
          index = i;
          break;
        }
      }
      if (index != -1) {
        result.append(latinTranslit[index]);
      } else {
        result.append(c);
      }
    }

    return result.toString();
  }
}
