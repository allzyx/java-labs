// Выровнять строку по середине до указанной ширины, дополнив слева и справа
// пробелами

import java.util.Scanner;

public class Main {
  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);

    // Ask the user to enter a sentence
    System.out.print("Enter a sentence: ");
    String inputString = scanner.nextLine();

    // Ask the user to specify the width
    System.out.print("Enter the desired width: ");
    int desiredWidth = scanner.nextInt(); // 103 is the width of the screen

    String centeredString = centerString(inputString, desiredWidth);
    System.out.println("'" + centeredString + "'");

    scanner.close();
  }

  public static String centerString(String input, int width) {

    input = input.trim();

    if (input.length() >= width) { // If string is long enough don't do anything
      return input;
    }

    int totalPadding = width - input.length();
    int leftPadding = totalPadding / 2;
    int rightPadding = totalPadding - leftPadding;

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < leftPadding; i++) {
      sb.append(" "); // Add spaces to the left
    }

    sb.append(input); // Add the string

    for (int i = 0; i < rightPadding; i++) {
      sb.append(" "); // Add spaces to the right
    }

    return sb.toString();
  }
}
