import java.util.Scanner;

public class AddSpaces_11 {
  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter a string: ");
    String originalString = scanner.nextLine();
    System.out.println("Enter the desired width: ");
    int desiredWidth = scanner.nextInt();

    String paddedString = leftPad(originalString, desiredWidth);

    System.out.println("Original String: " + originalString);
    System.out.println("Padded String: " + paddedString);
  }

  public static String leftPad(String original, int width) {
    if (original.length() >= width) {
      return original;
    } else {
      int spacesToAdd = width - original.length();

      StringBuilder paddedStringBuilder = new StringBuilder();

      for (int i = 0; i < spacesToAdd; i++) {
        paddedStringBuilder.append(' ');
      }

      paddedStringBuilder.append(original);
      return paddedStringBuilder.toString();
    }
  }
}
