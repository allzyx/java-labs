// Дополнить строку пробелами между словами до указанной ширины

// Import Scanner
import java.util.Scanner;

public class SpacesWithUserWidth_2 {
  public static void main(String[] args) {

    // Create a Scanner object to get user input
    Scanner scanner = new Scanner(System.in);

    // Ask the user to enter a sentence
    System.out.print("Enter a sentence: ");
    String inputString = scanner.nextLine();

    // Ask the user to specify the number of spaces
    System.out.print("Enter the number of spaces to divide by: ");
    int numSpaces = scanner.nextInt();

    // Split the sentence into words deleting any spaces
    String[] words = inputString.split("\\s+");

    StringBuilder newString = new StringBuilder();

    for (int i = 0; i < words.length; i++) {

      if (i < words.length) {

        //  Add word
        newString.append(words[i]);

        // Add spaces
        for (int j = 0; j < numSpaces; j++) {
          newString.append(" ");
        }
      }
    }

    String trimedString = newString.toString();
    trimedString = trimedString.trim();

    // Display old and new string
    System.out.println("Original string: '" + inputString + "'");
    System.out.println("New string: '" + trimedString + "'");

    // Close the scanner
    scanner.close();
  }
}

// StringBuilder is a alternative class to String
// it creates mutable sequences of characters, while String is immutable
//
// most used methods:
//
// insert(int offset, String str) Inserts a string at the specified offset
// within the specified offset
//
// delete(int start, int end) - deletes characters within the specified range
//
// replace(int start, int end, String str) - replaces characters within the
// specified range
//
// append(String str) - appends a string to the end of the StringBuilder
//
// setCharAt(int index, char ch) - sets the character at the specified index
//
// reverse()  - reverses characters
//
// toString() - converts StringBuilder to String

// Strings objects have a split() method that divides a string into
// substrings. You have to specify regex to match symbols to end substring
// "\\s+" means any number of spaces
