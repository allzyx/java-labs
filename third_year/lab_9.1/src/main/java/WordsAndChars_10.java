import java.util.Scanner;

public class SentenceAnalyzer {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter a sentence: ");
    String inputSentence = scanner.nextLine();
    scanner.close();

    // Count number of characters
    int charCount = inputSentence.length();

    // Divide sentence into words
    inputSentence = inputSentence.trim();
    String[] words = inputSentence.split("\\s+");

    // Count number of words
    int wordCount = words.length;

    // Find shortest and longest words
    String shortestWord = "";
    String longestWord = "";

    for (String word : words) {
      if (shortestWord.isEmpty() || word.length() < shortestWord.length()) {
        shortestWord = word;
      } else if (word.length() > longestWord.length()) {
        longestWord = word;
      }
    }

    // Вывод результатов
    System.out.println("Number of characters: " + charCount);
    System.out.println("Number of words: " + wordCount);
    System.out.println("Shortest word: " + shortestWord);
    System.out.println("Longest word: " + longestWord);
  }
}
