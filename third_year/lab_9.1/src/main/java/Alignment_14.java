import java.util.Scanner;

public class TextAlignment {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter text: ");
    String text = scanner.nextLine();

    System.out.println("Choose alignment type:");
    System.out.println("1. Left align");
    System.out.println("2. Rigth align");
    System.out.println("3. Center align");
    System.out.println("4. Justify align");
    int alignmentType = scanner.nextInt();

    switch (alignmentType) {
    case 1:
      alignLeft(text);
      break;
    case 2:
      alignRight(text);
      break;
    case 3:
      alignCenter(text);
      break;
    case 4:
      alignJustify(text);
      break;
    default:
      System.out.println("Invalid alignment type");
    }

    scanner.close();
  }

  public static void alignLeft(String text) {
    text = text.trim();
    System.out.println(text);
  }

  public static void alignRight(String text) {
    int totalWidth = 103;
    text = text.trim();
    String alignedText = String.format("%" + totalWidth + "s", text);
    System.out.println(alignedText);
  }

  public static void alignCenter(String text) {
    int totalWidth = 103;
    text = text.trim();
    String alignedText = String.format("%" + totalWidth + "s", text);
    int padding = (alignedText.length() - text.length()) / 2;
    System.out.println(alignedText.substring(padding));
  }

  public static void alignJustify(String text) {
    int totalWidth = 103;
    text = text.trim();
    int textLength = text.length();
    int spaceCount = totalWidth - textLength;
    String[] words = text.split("\\s+");
    int numberOfSpaces = words.length - 1;
    int widthOfSpace = spaceCount / numberOfSpaces;
    StringBuilder alignedText = new StringBuilder();

    for (String word : words) {

      alignedText.append(word);

      for (int i = 0; i < widthOfSpace; i++) {
        alignedText.append(" ");
      }
    }

    System.out.println(alignedText.toString().trim());
  }
}
