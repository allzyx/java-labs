import java.util.Scanner;

public class Ceaser_6 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter your sentence: ");
    String sentence = scanner.nextLine();
    int shift = 0;
    System.out.print("Enter the shift: ");
    shift = scanner.nextInt();
    scanner.close();

    String encryptedSentence = cipher(sentence, shift);           // Encrypt
    String decryptedSentence = cipher(encryptedSentence, -shift); // Decrypt

    System.out.println("Sentence after encryption: " + encryptedSentence);
    System.out.println("Sentence after decryption: " + decryptedSentence);
  }

  public static String cipher(String input, int shift) {
    StringBuilder result = new StringBuilder();

    for (char c : input.toCharArray()) {
      if (Character.isLetter(c)) {
        char base = Character.isLowerCase(c) ? 'a' : 'A';
        char encryptedChar = (char)(base + (c - base + shift + 26) % 26);
        result.append(encryptedChar);
      } else {
        result.append(c);
      }
    }

    return result.toString();
  }
}
