import java.util.Scanner;

public class DivideWords_5 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter your sentence: ");
    String source = scanner.nextLine();

    String[] words = source.split("\\s+");

    System.out.print("\nSource string: " + source + "\n\n");
    for (String word : words) {
      System.out.println(word);
    }

    scanner.close();
  }
}
