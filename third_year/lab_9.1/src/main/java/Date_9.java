import java.util.Scanner;

public class SplitDate {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter date in format DD.MM.YYYY: ");
    String inputDate = scanner.nextLine();
    scanner.close();

    String[] parts = inputDate.split("\\."); // Divide line by dots

    if (parts.length == 3) {
      String day = parts[0];
      String month = parts[1];
      String year = parts[2];

      System.out.println("Day: " + day);
      System.out.println("Month: " + month);
      System.out.println("Year: " + year);
    } else {
      System.out.println("Invalid date! Enter date in format DD.MM.YYYY.");
    }
  }
}
