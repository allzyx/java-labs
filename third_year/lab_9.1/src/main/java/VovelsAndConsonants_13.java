import java.util.*;

public class CountVowelsConsonants {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter a string: ");
    String input = scanner.nextLine();
    input = input.toLowerCase();

    int vowelCount = 0;
    int consonantCount = 0;

    char[] vowels = {'a', 'e', 'i', 'o', 'u'};
    char[] consonants = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n',
                         'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};

    for (int i = 0; i < input.length(); i++) {
      char ch = input.charAt(i);

      if (Character.isLetter(ch)) {
        for (int j = 0; j < vowels.length; j++) {
          if (ch == vowels[j]) {
            vowelCount++;
          }
        }
        for (int j = 0; j < consonants.length; j++) {
          if (ch == consonants[j]) {
            consonantCount++;
          }
        }
      }
    }

    System.out.println("Количество гласных: " + vowelCount);
    System.out.println("Количество согласных: " + consonantCount);
  }
}
