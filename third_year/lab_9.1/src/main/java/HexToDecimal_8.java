import java.util.Scanner;

public class HexToDecimal {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter a hex number: ");
    String hexString = scanner.nextLine();
    scanner.close();

    try {
      // Convert the hex string to an decimal integer
      int decimalValue = Integer.parseInt(hexString, 16);
      System.out.println("Decimal value: " + decimalValue);

    } catch (NumberFormatException e) {
      System.out.println("Некорректный ввод. ");
    }
  }
}
