// Удалить все ведущие и замыкающие пробелы в строке

// Import scanner
import java.util.Scanner;

public class RemoveSpaces {
  public static void main(String[] args) {

    // Create Scanner object
    Scanner scanner = new Scanner(System.in);

    // Prompt the user to enter a string
    System.out.print("Enter a string: ");
    String inputString = scanner.nextLine();

    // Remove spaces from the start and end of the string
    String trimmed = inputString.trim();

    // Display the input string and trimmed string
    System.out.println("Original string: '" + inputString + "'");
    System.out.println("Trimmed string: '" + trimmed + "'");
  }
}

// String objects have a trim() method that deletes leading and trailing
// spaces
