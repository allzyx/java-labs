import java.util.Scanner;

public class CapitalWords_4 {
  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter your sentence: ");
    String source = scanner.nextLine();

    String result = capitalizeWords(source);
    System.out.println("Modified String: " + result);

    scanner.close();
  }

  public static String capitalizeWords(String source) {

    String[] words = source.split("\\s+");

    StringBuilder resultBuilder = new StringBuilder();

    for (String word : words) {
      if (word.length() > 0) {
        String capitalizedWord =
            word.substring(0, 1).toUpperCase() + word.substring(1);

        resultBuilder.append(capitalizedWord).append(" ");
      }
    }

    return resultBuilder.toString().trim();
  }
}
